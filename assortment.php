<?php
 include_once("top.html");
 ?>
 
 <link rel="stylesheet" href="assortStyle.css" type="text/css">
 
<div class="content">
<?php
require_once 'connection.php';
$link = mysqli_connect($host, $userDB, $passwordDB, $database) 
        or die("Ошибка " . mysqli_error($link)); 

	$query="Select * FROM games";

    $result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link)); 
    if($result && mysqli_num_rows($result)>0) 
    {
		while($row = mysqli_fetch_object($result)) {
			
        $gameName = $row->gameName;
		$price=$row->price;	
		$content =$row->gameDescr;
		$imageRef=$row->gameImg;
         
		echo "<div class='product'>",
		"<div class='product-img'> <img src='$imageRef' alt='альтернативный текст'/></div>",
		"<div class='product-title'>$gameName</div>",
		"<div class='product-price'>$price Руб.</div>",
         "<p class='product-desc'>$content</p>", 
		 "<button>Купить</button></div>";
		}
		  		  
        mysqli_free_result($result);
    }
mysqli_close($link);
?>
</div>
</body>
</html>